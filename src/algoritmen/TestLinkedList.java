package algoritmen;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestLinkedList {
	
	LinkedList<String> ll2;

	@BeforeEach
	void setUp() throws Exception {
		ll2 = new LinkedList<String>("hallo");
	}

	@Test
	void testGetHead() {
		Node<String> head = new Node<String>("hallo");
		assertEquals(head, ll2.getHead());
	}

	@Test
	void testPrepend() {
		fail("Not yet implemented");
	}

	@Test
	void testGetSize() {
		LinkedList<String> ll = new LinkedList<String>();
		assertEquals(0,ll.getSize());
		assertEquals(1,ll2.getSize());
	}

}
