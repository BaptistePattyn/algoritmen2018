package algoritmen;
/**
 * This is a node containing an element used in a singly linked list
 * @author patty
 *
 * @param <E>
 * @param element 
 */
public class Node<E> {
	private E element;
	private Node<E> next;
	
	public Node (E element){
		this(element,null);
	}
	
	public Node(E element, Node<E> next) {
		this.element = element;
		this.next = next;
	}
	
	public E getElement() {
		return element;
	}
	
	public Node<E> getNext(){
		return next;
	}
	
	public void setNext(Node<E> next) {
		this.next = next;
	}
	
}
